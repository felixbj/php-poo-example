<?php
    function __autoload($sopa) {
        require_once $sopa . '.php';
    }
    $cor = array(
        array(
            array("O","I","E"),
            array("I","I","X"),
            array("E","X","E"),
        ),
        array(
            array("E","I","O","I","E","I","O","E","I","O")
        ),
        array(
            array("E","A","E","A","E"),
            array("A","I","I","I","A"),
            array("E","I","O","I","E"),
            array("A","I","I","I","A"),
            array("E","A","E","A","E")
        ),
        array(
            array("O","X"),
            array("I","O"),
            array("E","X"),
            array("I","I"),
            array("O","X"),
            array("I","E"),
            array("E","X")
        )
    );
    if ($_POST) {
        try {
            $matriz = $cor[json_decode($_POST['json'])->value - 1];
            $sopa = new Sopa($matriz);
            $palabra = "OIE";
        } catch (\Throwable $th) {
            $response = json_encode(["respuesta" => "Algo salió mal","code" => 500]);
            echo $response;
        }
        $response = json_encode([
            "respuesta" => $matriz,
            "horizontal" => $sopa->countFindWordHotizontal($palabra),
            "vertical" => $sopa->countFindWordVertical($palabra),
            "diagonalp" => $sopa->countFindWordDiagonalP($palabra),
            "diagonals" => $sopa->countFindWordDiagonalS($palabra),
            "code" => 200
        ]);
        echo $response;
    }
?>