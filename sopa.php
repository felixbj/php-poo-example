<?php
    class Sopa{
        private $rows;
        private $cols;
        private $diagonales_n;
        private $diagonales_i;
        private $horizontales;
        private $verticales;

        public function __construct(&$matriz) {
            $this->rows = count($matriz);
            $this->cols = count($matriz[0]); 
            $this->diagonales_n = $this->diagonales_i = $this->horizontales = $this->verticales = array();
            $this->getAllString($matriz);
        }
        private function getAllString(&$matriz){
            //OBTENER TODAS LAS CADENAS DE LA SOPA
            for ($i=0,$size_rows = $this->rows; $i < $size_rows; $i++) {
                array_push($this->horizontales,""); 
                for ($j=0,$size_cols = $this->cols; $j < $size_cols; $j++) {
                    //HORIZONTALES
                    $this->horizontales[$i].=$matriz[$i][$j];
                    //VERTICALES
                    ($i==0) ? array_push($this->verticales,$matriz[$i][$j]) : $this->verticales[$j].=$matriz[$i][$j];
                    //DIAGONALES PRINCIPALES
                    if(($j-$i) >= 0)//arriba de la principal
                        (isset($this->diagonales_n[$j-$i])) ?  $this->diagonales_n[$j-$i].=$matriz[$i][$j] : array_push($this->diagonales_n,$matriz[$i][$j]);
                    else//abajo de la principal
                        (isset($this->diagonales_n[$i-$j+$size_cols-1])) ? $this->diagonales_n[$i-$j+$size_cols-1].=$matriz[$i][$j] : array_push($this->diagonales_n,$matriz[$i][$j]);
                    //DIAGONALES SECUNDARIAS
                    (isset($this->diagonales_i[$i+$j])) ? $this->diagonales_i[$i+$j] = $matriz[$i][$j].$this->diagonales_i[$i+$j] : array_push($this->diagonales_i,$matriz[$i][$j]);
                }
            }            
        }

        public function countFindWordHotizontal($palabra){
            $apariciones = 0;
            for ($i=0; $i < count($this->horizontales) ; $i++) {
                if(!(strlen($palabra) > strlen($this->horizontales[$i]))){//descartar por tamano
                    $apariciones += substr_count($this->horizontales[$i], $palabra);//Cuenta el número de apariciones del substring
                    $apariciones += substr_count(strrev($this->horizontales[$i]), $palabra);//Invierte una string
                } 
            }
            return $apariciones;
        }
        public function countFindWordVertical($palabra){
            $apariciones = 0;
            for ($i=0; $i < count($this->verticales) ; $i++) {
                if(!(strlen($palabra) > strlen($this->verticales[$i]))){//descartar por tamano
                    $apariciones += substr_count($this->verticales[$i], $palabra);//Cuenta el número de apariciones del substring
                    $apariciones += substr_count(strrev($this->verticales[$i]), $palabra);//Invierte una string
                } 
            }
            return $apariciones;
        }
        public function countFindWordDiagonalP($palabra){
            $apariciones = 0;
            for ($i=0; $i < count($this->diagonales_n) ; $i++) {
                if(!(strlen($palabra) > strlen($this->diagonales_n[$i]))){//descartar por tamano
                    $apariciones += substr_count($this->diagonales_n[$i], $palabra);//Cuenta el número de apariciones del substring
                    $apariciones += substr_count(strrev($this->diagonales_n[$i]), $palabra);//Invierte una string
                } 
            }
            return $apariciones;
        }
        public function countFindWordDiagonalS($palabra){
            $apariciones = 0;
            for ($i=0; $i < count($this->diagonales_i) ; $i++) {
                if(!(strlen($palabra) > strlen($this->diagonales_i[$i]))){//descartar por tamano
                    $apariciones += substr_count($this->diagonales_i[$i], $palabra);//Cuenta el número de apariciones del substring
                    $apariciones += substr_count(strrev($this->diagonales_i[$i]), $palabra);//Invierte una string
                } 
            }
            return $apariciones;
        }

    }
?>